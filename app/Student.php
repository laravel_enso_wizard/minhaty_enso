<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelEnso\Tables\app\Traits\TableCache;

class Student extends Model
{
    
	protected $fillable = [
        'fname', 'lname', 'cne','email','naissance','cni','city','yatim'
    ];
}

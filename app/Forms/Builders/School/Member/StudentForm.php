<?php

namespace App\Forms\Builders\School\Member;

use App\Student;
use LaravelEnso\Forms\app\Services\Form;

class StudentForm
{
    protected const TemplatePath = __DIR__.'/../../../Templates/School/Member/student.json';

    protected $form;

    public function __construct()
    {
        $this->form = new Form(static::TemplatePath);
    }

    public function create()
    {
        return $this->form->create();
    }

    public function edit(Student $student)
    {
        return $this->form->edit($student);
    }
}

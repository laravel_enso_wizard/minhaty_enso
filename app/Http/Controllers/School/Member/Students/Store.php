<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Student;
use Illuminate\Routing\Controller;
use App\Http\Requests\School\Member\ValidateStudentRequest;

class Store extends Controller
{
    public function __invoke(ValidateStudentRequest $request, Student $student)
    {
        $student->fill($request->validated())->save();

        return [
            'message' => __('The student was successfully created'),
            'redirect' => 'school.member.students.edit',
            'param' => ['student' => $student->id],
        ];
    }
}

<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Student;
use Illuminate\Routing\Controller;
use App\Forms\Builders\School\Member\StudentForm;

class Edit extends Controller
{
    public function __invoke(Student $student, StudentForm $form)
    {
        return ['form' => $form->edit($student)];
    }
}

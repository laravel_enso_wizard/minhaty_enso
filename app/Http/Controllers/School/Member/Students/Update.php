<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Student;
use Illuminate\Routing\Controller;
use App\Http\Requests\School\Member\ValidateStudentRequest;

class Update extends Controller
{
    public function __invoke(ValidateStudentRequest $request, Student $student)
    {
        $student->update($request->validated());

        return ['message' => __('The student was successfully updated')];
    }
}

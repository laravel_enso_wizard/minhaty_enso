<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Student;
use Illuminate\Routing\Controller;

class Destroy extends Controller
{
    public function __invoke(Student $student)
    {
        $student->delete();

        return [
            'message' => __('The student was successfully deleted'),
            'redirect' => 'school.member.students.index',
        ];
    }
}

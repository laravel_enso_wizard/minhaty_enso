<?php

use LaravelEnso\Migrator\app\Database\Migration;

class CreateStructureForStudents extends Migration
{
    protected $permissions = [
        ['name' => 'school.member.students.index', 'description' => 'Show index for students', 'type' => 0, 'is_default' => false],

        ['name' => 'school.member.students.create', 'description' => 'Create student', 'type' => 1, 'is_default' => false],
        ['name' => 'school.member.students.store', 'description' => 'Store a new student', 'type' => 1, 'is_default' => false],
        ['name' => 'school.member.students.show', 'description' => 'Show student', 'type' => 1, 'is_default' => false],
        ['name' => 'school.member.students.edit', 'description' => 'Edit student', 'type' => 1, 'is_default' => false],
        ['name' => 'school.member.students.update', 'description' => 'Update student', 'type' => 1, 'is_default' => false],
        ['name' => 'school.member.students.destroy', 'description' => 'Delete student', 'type' => 1, 'is_default' => false],
        ['name' => 'school.member.students.initTable', 'description' => 'Init table for students', 'type' => 0, 'is_default' => false],

        ['name' => 'school.member.students.tableData', 'description' => 'Get table data for students', 'type' => 0, 'is_default' => false],

        ['name' => 'school.member.students.exportExcel', 'description' => 'Export excel for students', 'type' => 0, 'is_default' => false],

        ['name' => 'school.member.students.options', 'description' => 'Get student options for select', 'type' => 0, 'is_default' => false],
    ];

    protected $menu = [
        'name' => 'Students', 'icon' => 'people', 'route' => 'school.member.students.index', 'order_index' => 100, 'has_children' => false
    ];

    protected $parentMenu = 'Administration';
}


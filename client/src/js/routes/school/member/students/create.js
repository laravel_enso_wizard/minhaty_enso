const StudentCreate = () =>
    import ('./../../../../pages/school/member/students/Create.vue');

export default {
    name: 'school.member.students.create',
    path: 'create',
    component: StudentCreate,
    meta: {
        breadcrumb: 'create',
        title: 'Create Student',
    },
};
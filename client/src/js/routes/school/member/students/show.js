const StudentShow = () =>
    import ('./../../../../pages/school/member/students/Show.vue');

export default {
    name: 'school.member.students.show',
    path: ':student',
    component: StudentShow,
    meta: {
        breadcrumb: 'show',
        title: 'Show Student',
    },
};
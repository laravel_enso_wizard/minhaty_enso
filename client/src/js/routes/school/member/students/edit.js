const StudentEdit = () =>
    import ('./../../../../pages/school/member/students/Edit.vue');

export default {
    name: 'school.member.students.edit',
    path: ':student/edit',
    component: StudentEdit,
    meta: {
        breadcrumb: 'edit',
        title: 'Edit Student',
    },
};
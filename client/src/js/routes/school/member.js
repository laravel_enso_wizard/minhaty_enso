import routeImporter from '@core/modules/importers/routeImporter';

const routes = routeImporter(require.context('./member', false, /.*\.js$/));
const RouterView = () => import('@core/bulma/pages/Router.vue');

export default {
    path: 'member',
    component: RouterView,
    meta: {
        breadcrumb: 'member',
    },
    children: routes,
};
